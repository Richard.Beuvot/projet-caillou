# Descriptif

Ce programme définit une API Rest capable de récupérer des informations d'open food fact, selectionner celles qui sont 
importantes et vous les fournir via des appels REST

# Utilisation 

Notre Api défini trois appels différents : 

GET sur /food/product?code_barre= "..."

Cet appel permet de récupérer des informations sur un produits en particulier ses composantes négatives et positives 

PUT sur /food/product?code_barre= "..." & user= "..." 

Cet appel permet d'ajouter un produit d'un certain au panier d'un certain utilisateur 

GET sur /food/panier?user

Cet appel permet de récupérer le contenu du panier d'un certain utilisateur qui a été rempli avec la commande ci dessus,
les produits reviennent sous le même format qu'avec le get sur /food/product 