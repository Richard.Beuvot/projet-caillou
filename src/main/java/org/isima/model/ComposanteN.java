package org.isima.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ComposanteN {
    DENSNRJ("Densité énérgétique"),GRAISSES("Graisses saturées")
    ,SUCRES("Sucres simples"),SODIUM("Sodium1");
    private String nom;
    private int pts;

    private static String composanteNJsonFilePath = "./ressources/files/composantesN.json";

    ComposanteN(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public int getPts() {
        return pts;
    }

    // Permet de récupérer les points correspondant a une composante
    // négative
    public void getPointsForCP(float val100g){
        pts =  Composante.readPointsFromJson(composanteNJsonFilePath,this.nom,val100g);
    }

    @Override
    public String toString() {
        return "ComposanteN{" +
                "nom='" + nom + '\'' +
                ", pts=" + pts +
                '}';
    }
}
