package org.isima.model;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Composante {
    public static int readPointsFromJson(String filename, String composantName, float val){
        int pts = -1;
        try {
            int i = 0;
            InputStream is = new FileInputStream(filename);

            JSONTokener tokener = new JSONTokener(is);
            JSONObject object = new JSONObject(tokener);
            JSONArray composantArray = object.getJSONArray(composantName);

            for(i = 0; i<composantArray.length(); i++){
                if(val < composantArray.getDouble(i)) {
                    pts = i;
                    break;
                }
            }

            if(pts == -1)
                pts=composantArray.length();

        } catch (FileNotFoundException e){
            e.printStackTrace();
        }

        return pts;
    }
}
