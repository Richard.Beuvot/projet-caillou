package org.isima.model;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductFactory {
    public Produit setComposantes(String codeBarre, String nom, JSONObject nutriments){
        List<ComposanteN> composanteNList = new ArrayList<>();
        List<ComposanteP> composantePList = new ArrayList<>();

        ComposanteP proteines = ComposanteP.PROTÉINES;
        proteines.getPointsForCP((float)nutriments.getDouble("proteins_100g"));
        composantePList.add(proteines);

        ComposanteP fibres = ComposanteP.FIBRES;
        fibres.getPointsForCP((float)nutriments.getDouble("fiber_100g"));
        composantePList.add(fibres);

        ComposanteN energie = ComposanteN.DENSNRJ;
        energie.getPointsForCP((float)nutriments.getDouble("energy_100g"));
        composanteNList.add(energie);

        ComposanteN graisses = ComposanteN.GRAISSES;
        graisses.getPointsForCP((float)nutriments.getDouble("fat_100g"));
        composanteNList.add(graisses);

        ComposanteN Sucres = ComposanteN.SUCRES;
        Sucres.getPointsForCP((float)nutriments.getDouble("sugars_100g"));
        composanteNList.add(Sucres);

        ComposanteN sodium1 = ComposanteN.SODIUM;
        sodium1.getPointsForCP((float)nutriments.getDouble("sodium_100g"));
        composanteNList.add(sodium1);

        return new Produit(codeBarre, nom, composantePList, composanteNList);

    }

    public Produit makeProduit(String codeBarre){
        JSONObject jsonObject = null;
        String nom = "";
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get("https://fr.openfoodfacts.org/api/v0/produit/" + codeBarre + ".json").asJson();
            jsonObject = jsonResponse.getBody().getObject();

            nom = jsonObject.getJSONObject("product").getString("product_name");
            JSONObject nutriments = jsonObject.getJSONObject("product").getJSONObject("nutriments");
            return setComposantes(codeBarre, nom, nutriments);
        } catch (UnirestException e){
            e.printStackTrace();
        }
        return null;
    }
}
