package org.isima.model;

import java.util.List;

public class Produit {
    private String code_barre;
    private String nom_produit;
    private List<ComposanteP> composantePList;
    private List<ComposanteN> composanteNList;

    public Produit(String code_barre, String nom_produit, List<ComposanteP> composantePList, List<ComposanteN> composanteNList) {
        this.code_barre = code_barre;
        this.nom_produit = nom_produit;
        this.composantePList = composantePList;
        this.composanteNList = composanteNList;
    }

    public String getCode_barre() {
        return code_barre;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public List<ComposanteP> getComposantePList() {
        return composantePList;
    }

    public List<ComposanteN> getComposanteNList() {
        return composanteNList;
    }
}
