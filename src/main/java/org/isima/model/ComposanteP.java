package org.isima.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ComposanteP {
    FIBRES("Fibres"),PROTÉINES("Protéines");
    private String nom;
    private int pts;

    private static String composantePJSONFilePath = "./ressources/files/composantesP.json";

    ComposanteP(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public int getPts() {
        return pts;
    }

    // Permet de récupérer les points correspondant a une composante
    // positive
    public void getPointsForCP(float val100g){
        pts = Composante.readPointsFromJson(composantePJSONFilePath,this.nom,val100g);
    }

    @Override
    public String toString() {
        return "ComposanteP{" +
                "nom='" + nom + '\'' +
                ", pts=" + pts +
                '}';
    }
}
