package org.isima.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Panier {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String userId;
    @ElementCollection
    private List<String> codeBarres;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getCodeBarres() {
        return codeBarres;
    }

    public void addCodeBarre(String codeBarre){
        if(codeBarres == null)
            codeBarres = new ArrayList<>();
        codeBarres.add(codeBarre);
    }
}
