package org.isima.service;

import org.isima.model.Panier;
import org.isima.repository.RepositoryPanier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PanierService {

    @Autowired
    private RepositoryPanier paniersDB;

    public void saveOrUpdate(Panier panier){
        paniersDB.save(panier);
    }

    public List<Panier> getDataByUserId(String userId){
        return paniersDB.findByUserId(userId);
    }
}
