package org.isima.controller;

import org.isima.model.Panier;
import org.isima.model.ProductFactory;
import org.isima.model.Produit;
import org.isima.service.PanierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/food")
public class FoodController {
    @Autowired
    PanierService panierService;

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public ResponseEntity getFoodInformation(@RequestParam(value="code_barre") String codeBarre){
        ProductFactory pf = new ProductFactory();
        Produit p = pf.makeProduit(codeBarre);

        if(p!=null)
            return ResponseEntity.ok(p);
        return ResponseEntity.status(404).body("Product could not be found");
    }

    @RequestMapping(value = "/product", method = RequestMethod.PUT)
    public ResponseEntity addProductToPanier(@RequestParam(value="code_barre") String codeBarre,
                                             @RequestParam(value="user") String userId){
        ProductFactory pf = new ProductFactory();
        Produit p = pf.makeProduit(codeBarre);

        Panier panier = new Panier();
        panier.setUserId(userId);
        panier.addCodeBarre(codeBarre);
        panierService.saveOrUpdate(panier);

        if(p!=null)
            return ResponseEntity.ok(p);
        return ResponseEntity.status(404).body("Product could not be found");
    }

    @RequestMapping(value = "/panier", method = RequestMethod.GET)
    public ResponseEntity getPanier(@RequestParam(value="user") String userId){
        List<Panier> panierList = panierService.getDataByUserId(userId);
        List<Produit> produitList = new ArrayList<>();
        ProductFactory pf = new ProductFactory();

        for (Panier panier : panierList){
            if(panier.getCodeBarres() != null){
                for(String s : panier.getCodeBarres()){
                    Produit p = pf.makeProduit(s);
                    if(p != null)
                        produitList.add(p);
                }
            }
        }

        return ResponseEntity.ok(produitList);
    }
}
