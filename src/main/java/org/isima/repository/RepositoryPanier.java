package org.isima.repository;

import org.isima.model.Panier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryPanier extends CrudRepository<Panier, Long> {
    List<Panier> findByUserId(String userId);
}
