package org.isima.model;

import org.junit.Test;

import static org.junit.Assert.*;

// Impossble de tester la librairie org.Json a cause de problèmes de compatibilité
public class ComposanteTest {

    @Test
    public void readPointsFromJson() {
        ComposanteN cn = ComposanteN.GRAISSES;
        ComposanteP cp = ComposanteP.FIBRES;

        int pt1 = Composante.readPointsFromJson("./ressources/files/composantesN.json", cn.getNom(), 0);
        assertEquals(0,pt1);

    }
}