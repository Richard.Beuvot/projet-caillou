package org.isima.controller;

import java.io.UnsupportedEncodingException;

import org.isima.ProjetCaillouApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = ProjetCaillouApplication.class)
public class FoodControllerTest {
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getFoodInformation() {
        String uri = "/food/product?code_barre=3029330003533";
        MvcResult mvcResult = null;
        try {
            mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = null;
        try {
            content = mvcResult.getResponse().getContentAsString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assertEquals(content, "{\"code_barre\":\"3029330003533\",\"nom_produit\":\"Crousti Moelleux Complet\",\"composantePList\":[{\"nom\":\"Protéines\",\"pts\":5},{\"nom\":\"Fibres\",\"pts\":5}],\"composanteNList\":[{\"nom\":\"Densité énérgétique\",\"pts\":3},{\"nom\":\"Graisses saturées\",\"pts\":4},{\"nom\":\"Sucres simples\",\"pts\":1},{\"nom\":\"Sodium1\",\"pts\":0}]}");
    }

    @Test
    public void addProductToPanier() {
        String uri = "/food/product?code_barre=3029330003533&user=toto";
        MvcResult mvcResult = null;
        try {
            mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = null;
        try {
            content = mvcResult.getResponse().getContentAsString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assertEquals(content, "{\"code_barre\":\"3029330003533\",\"nom_produit\":\"Crousti Moelleux Complet\",\"composantePList\":[{\"nom\":\"Protéines\",\"pts\":5},{\"nom\":\"Fibres\",\"pts\":5}],\"composanteNList\":[{\"nom\":\"Densité énérgétique\",\"pts\":3},{\"nom\":\"Graisses saturées\",\"pts\":4},{\"nom\":\"Sucres simples\",\"pts\":1},{\"nom\":\"Sodium1\",\"pts\":0}]}");

    }

    @Test
    public void getPanier() {
        String uri = "/food/panier?user=toto";
        MvcResult mvcResult = null;
        try {
            mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = null;
        try {
            content = mvcResult.getResponse().getContentAsString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assertEquals(content, "[]");
    }
}